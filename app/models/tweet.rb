class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, :length => { :maximum => 180 } 
  # validates :latest_tweet_equal

  def latest_tweet_equal
    return false if Tweet.where(body: body, user_id: user_id).where("Date(created_at) > ?", Date.today.yesterday).present?

    true
  end
end
